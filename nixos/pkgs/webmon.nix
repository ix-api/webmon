{...}:
let
  pkgs = import <nixpkgs> {};
  buildRustPackage = pkgs.rustPlatform.buildRustPackage;
  ng = pkgs.nix-gitignore;
in
buildRustPackage rec {
  pname = "webmon";
  version = "0.1.0";
  src = ng.gitignoreSource [] ../..;

  cargoSha256 = "sha256-fViYNZUQwlD2LK7/W4/7fQE2eciylYg2ttzeXHLLato=";

  depsBuildBuild = with pkgs; [
    pkg-config
    openssl
  ];
}
