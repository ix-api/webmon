{config, lib, ...}:
let
  pkgs = import <nixpkgs> {};
  webmon = with pkgs; callPackage ./pkgs/webmon.nix {};

  cfg = config.services.webmon;

  options = with lib; with types; {
    enable = mkEnableOption {
      description = "Enable web monitoring";
    };
    listen = mkOption {
      description = "listen on this address";
      default = "localhost:22022";
      type = str;
    };
    urls = mkOption {
      description = "A list of urls to visit";
      type = listOf str;
    };

  };

  urlfile = with pkgs; with lib; writeTextFile {
    name = "urls.txt";
    text = concatStringsSep "\n" cfg.urls;
  };
in
{
  options.services.webmon = options;
  config = lib.mkIf cfg.enable {
  
    # Make systemd unit
    systemd.services.webmon = {
      wantedBy = ["multi-user.target"];
      after = ["network.target"];
      description = "IX-API Website Monitoring";
      serviceConfig = {
        Type = "simple";
        ExecStart = ''
          ${webmon}/bin/ix-api-webmon \
            -l ${cfg.listen} \
            -r ${urlfile}
        '';
      };
    };
  };
}
