
# Webmon

Periodically make HTTP requests and export the result using
prometheus.

Available metrics:

 - `webmon_response_size`: (`url`, `method`)
 - `webmon_response_duration`: (`url`, `method`)


