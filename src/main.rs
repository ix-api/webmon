use anyhow::Result;

mod api;
mod args;
mod metrics;
mod pkg;

use metrics::state::State;

#[tokio::main]
async fn main() -> Result<()> {
    let args = args::parse();

    println!("webmon\t\t\t\tv{}", pkg::VERSION);
    println!("  listen: {}", args.listen);

    // Start collecting metrics
    let state = State::spawn(&args).await;

    // Run the HTTP server
    api::start(&args, state).await
}
