use crate::{
    args::Args,
    metrics::sampler::{Sample, Sampler},
};
use std::sync::{Arc, Mutex};
use tokio::time::{sleep, Duration};

/// Collect metrics for prometheus export
#[derive(Default)]
pub struct State {
    metrics: Vec<Sample>,
}

/// Shared metrics state
pub type StateRef = Arc<Mutex<State>>;

impl State {
    /// Start collecting
    pub async fn spawn(args: &Args) -> StateRef {
        let state = Arc::new(Mutex::new(Self::default()));
        let filename = args.requests_filename.to_owned();

        {
            let state = state.clone();
            tokio::spawn(async move {
                loop {
                    println!("collecting metrics...");
                    let sampler = match Sampler::from_file(&filename) {
                        Ok(s) => s,
                        Err(err) => {
                            println!("ERROR! {}", err);
                            sleep(Duration::from_millis(1000)).await;
                            continue;
                        }
                    };

                    // Make requests
                    let results = sampler.sample().await;

                    // Update state
                    {
                        let mut state = state.lock().unwrap();
                        state.metrics = results;
                    }

                    sleep(Duration::from_secs(300)).await;
                }
            });
        }
        state
    }

    pub fn samples(&self) -> Vec<Sample> {
        self.metrics.clone()
    }
}
