use prometheus::{
    core::{Collector, Desc},
    proto::MetricFamily,
    GaugeVec, Opts,
};

use crate::metrics::state::StateRef;

pub struct Metrics {
    state: StateRef,

    res_size: GaugeVec,
    res_duration: GaugeVec,
}

impl Metrics {
    /// Create new metrics export
    pub fn new(state: StateRef) -> Self {
        macro_rules! metric {
            ($name:literal, $help:literal) => {
                GaugeVec::new(
                    Opts::new($name, $help),
                    &["url", "method", "code", "error"],
                )
                .unwrap()
            };
        }

        // Define metrics
        let res_size =
            metric!("webmon_response_size", "Size of the response in bytes");
        let res_duration = metric!(
            "webmon_response_duration",
            "Duration of the response in seconds"
        );

        Self {
            state,
            res_size,
            res_duration,
        }
    }
}

impl Collector for Metrics {
    /// Get labels
    fn desc(&self) -> Vec<&Desc> {
        let mut desc = self.res_size.desc();
        desc.append(&mut self.res_duration.desc());
        desc
    }

    fn collect(&self) -> Vec<MetricFamily> {
        let samples = {
            let state = self.state.lock().unwrap();
            state.samples()
        };
        // Clear metrics
        self.res_size.reset();
        self.res_duration.reset();

        // Fill metrics
        samples.iter().for_each(|sample| {
            self.res_size
                .with_label_values(&[
                    &sample.request.url,
                    "GET",
                    format!("{}", sample.code).as_ref(),
                    sample.error.clone().unwrap_or("".to_string()).as_ref(),
                ])
                .set(sample.size as f64);
            self.res_duration
                .with_label_values(&[
                    &sample.request.url,
                    "GET",
                    format!("{}", sample.code).as_ref(),
                    sample.error.clone().unwrap_or("".to_string()).as_ref(),
                ])
                .set(sample.duration.as_secs_f64());
        });

        // Collect
        let mut metrics = self.res_size.collect();
        metrics.append(&mut self.res_duration.collect());
        metrics
    }
}
