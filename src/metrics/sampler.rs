use anyhow::Result;
use bytes::Bytes;
use futures::future::join_all;
use reqwest::Client as HttpClient;
use std::{
    fs::File,
    io::{BufRead, BufReader},
    time::{Duration, Instant},
};

#[derive(Debug, Clone)]
pub struct Sample {
    pub size: usize,
    pub code: u16,
    pub duration: Duration,
    pub error: Option<String>,
    pub request: Request,
}

#[derive(Debug, Clone, Default)]
pub enum Method {
    #[default]
    GET,
}

#[derive(Debug, Clone, Default)]
pub struct Request {
    pub url: String,
    pub method: Method,
    pub payload: Option<String>,
}

impl Request {
    /// Parse line into request
    fn parse(line: &str) -> Option<Self> {
        // Check if the line is a comment
        if line.starts_with("#") {
            return None;
        }

        Some(Self {
            url: line.to_owned(),
            ..Self::default()
        })
    }

    /// Make the request and get metric
    pub async fn run(&self) -> Sample {
        let Self { url, .. } = self;

        println!("INFO! Sampling {}", url);

        let client = HttpClient::builder()
            .timeout(Duration::from_millis(60000))
            .build()
            .unwrap();

        let t0 = Instant::now();
        let response = client.get(url).send().await;
        let duration = t0.elapsed();

        match response {
            Err(err) => Sample {
                request: self.clone(),
                size: 0,
                code: 0,
                duration: duration,
                error: Some(format!("{}", err)),
            },
            Ok(res) => {
                let code = res.status().as_u16();
                let bytes = res.bytes().await.unwrap_or(Bytes::new());

                Sample {
                    request: self.clone(),
                    size: bytes.len(),
                    code: code,
                    duration: duration,
                    error: None,
                }
            }
        }
    }
}

/// Sampler for running requests
pub struct Sampler(Vec<Request>);

impl Sampler {
    /// Load requests from file
    pub fn from_file(filename: &str) -> Result<Self> {
        let file = File::open(filename)?;
        let lines = BufReader::new(file).lines();

        let requests: Vec<Request> = lines
            .filter_map(|l| {
                if let Ok(l) = l {
                    Request::parse(&l)
                } else {
                    None
                }
            })
            .collect();

        Ok(Self(requests))
    }

    /// Fetch results
    pub async fn sample(&self) -> Vec<Sample> {
        let Self(requests) = self;
        let results = requests.iter().map(|req| req.run());
        join_all(results).await
    }
}
