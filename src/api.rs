use actix_web::{
    web::{self},
    App, HttpResponse, HttpServer, Responder, Result as WebResult,
};
use actix_web_prom::PrometheusMetricsBuilder;
use anyhow::Result;

use crate::{
    args::Args,
    metrics::{export::Metrics, state::StateRef},
    pkg::VERSION,
};

/// Show a welcome page
async fn api_index() -> WebResult<impl Responder> {
    let content = format!("HTTPmon\tv.{}\n", VERSION);
    Ok(HttpResponse::Ok()
        .append_header(("Content-Type", "text/plain"))
        .body(content))
}

/// Start the API http server, exporting the metrics
pub async fn start(args: &Args, state: StateRef) -> Result<()> {
    // Configure prometheus metrics
    let prometheus = PrometheusMetricsBuilder::new("webmon")
        .endpoint("/api/v1/metrics")
        .build()
        .unwrap();
    prometheus
        .registry
        .register(Box::new(Metrics::new(state)))?;
    let server = HttpServer::new(move || {
        App::new()
            .wrap(prometheus.clone())
            .route("/", web::get().to(api_index))
    })
    .bind(args.listen.clone())?;
    server.run().await?;
    Ok(())
}
