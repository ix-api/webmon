use clap::Parser;

#[derive(Parser, Debug, Clone)]
#[command(author, version, about)]
pub struct Args {
    #[arg(short, long, default_value = "localhost:22022")]
    pub listen: String,

    #[arg(short, long, default_value = "requests.txt")]
    pub requests_filename: String,
}

/// Parse commandline arguments
pub fn parse() -> Args {
    Args::parse()
}
